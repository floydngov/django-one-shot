from django.urls import path
from todos.views import show_todo_list, todo_list_detail, create_todo_list, update_todo_list, delete_todo_list, todo_item_create, todo_item_update


urlpatterns = [
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", show_todo_list, name="todo_list_list"),
]
